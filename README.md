# Kapamilya Accounts Profile Enhancement

## Setup

#### Requirements
* NodeJS (https://nodejs.org/en/)
* Gulp (http://gulpjs.com/)
* VueJS (https://vuejs.org/)

To make sure you have Node, npm, and gulp installed, run three simple commands to see what version of each is installed:
* To see if Node is installed, type `node -v` in Terminal. This should print the version number.
* To see if NPM is installed, type `npm -v` in Terminal. This should print the version number.
* To see if gulp is installed, type `gulp -v` in Terminal. This should print the version number.

#### Installation
1. **Run npm for one-time installation of development dependencies**  
  `sudo npm install`

2. **Refer to the link below for vuejs turorials**
  https://vuejs.org/v2/guide/index.html
  https://medium.com/codingthesmartway-com-blog/vue-js-2-quickstart-tutorial-2017-246195cfbdd2

3. **Run gulp to compile and watch**  
  `npm start` or `npm run serve` - depend on how you set it up in your package.json file

## Development
```
$ Creating new pages/tabs - add a new file into `/src/views/` folder with the extension of .vue
$ `/src/styles/` - you can add your stylesheet/css inside this folder.
$ `/src/component/AccountLingking` this folder is where you can find the code for the account lingking process it also has seperated file for each accounts.
$ `/src/component/BasicInfo` this folder is contain all the basic info page in seperated component.
$ `/src/component/ContactType.vue` is the file where the Contact Views Process Starts.
$ `/src/component/Email` this folder is where you can add/delete/setAsPrimary of Email.
$ `/src/component/Mobile` this folder is where you can add/verify/delete/setAsPrimary of Mobile Number.
$ `/src/component/ModalDialog.vue` this file have the code where you can Edit/Upload a new profile picture/photo.
$ `/src/component/SideMenu.vue` this file have the code of SideBar menu.
``` 
## Store
```
`/src/store/`
$ `/src/store/index.js`
* This is where you can store all data coming from gigya accounts. 
$ `/src/store/vars.js`
* This is the variables declaration in store.
```
## Dependencies
```
* vee-select for dropdown
* croppieJS for image upload
* lodash.js to manipulate object data.
* simplebar for scrollbar design