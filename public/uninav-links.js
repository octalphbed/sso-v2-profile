var ssoUninav = {};
ssoUninav.show = function() {
	$('.my-blocker').show();
	$('html').css({'overflow':'hidden', '-webkit-overflow-scrolling':'touch'});
	stopScroll();

	$('.abs-cbn-links-container').addClass("uninav-links-active");
	$('.close-uninav-links').show();
};

ssoUninav.close = function() {
	$('.close-uninav-links').hide();
	$('.my-blocker').hide();
	$('html').css({'overflow':'auto', '-webkit-overflow-scrolling':'touch'});
	enableScroll();

	$('.abs-cbn-links-container').removeClass("uninav-links-active");
};


$(document).on('click', '.sso-network-uninav', function() {
	ssoUninav.show();
});

$(document).on('click', '.close-uninav-links, .my-blocker', function() {
	ssoUninav.close();
});

$(document).on('click', '.abs-cbn-links-container .uninav-abs-cbn .parent-uninav', function(e) {
	e.stopPropagation();
	if ($(this).text() !== "ABS-CBN") {
	  $(this).find('.toggle-uninav-links').trigger('click');
	}
  });
  
$(document).on('click', '.abs-cbn-links-container .uninav-abs-cbn .parent-uninav .toggle-uninav-links', function(e) {
  e.stopPropagation();
  var isActive = ($(this).hasClass('active')) ? true : false; // checks if it is already active

  $('.abs-cbn-links-container .uninav-abs-cbn .parent-uninav .toggle-uninav-links').removeClass("active");
  $('.uninav-container-links').find('.child-uninav').slideUp("fast");
  
  if (!isActive) {
	$(this).addClass("active");
	$(this).parents('.uninav-container-links').find('.child-uninav').slideToggle("fast");
  }
});

var disableScroll = false;
var scrollPos = 0;

function stopScroll() {
  disableScroll = true;
  scrollPos = $(window).scrollTop();
}
function enableScroll() {
  disableScroll = false;
}

$(function() {
	var markup = '';
		  
	$.getJSON("https://assets.abs-cbn.com/universalnav/uninav-links.json", function(data) {
		markup += '<div class="abs-cbn-links-container">';
			markup += '<div class="sso-uninav-link-title">';
				markup += 'EXPLORE ABS-CBN';
				markup += '<span class="fa fa-times close-uninav-links"></span>';
			markup += '</div>';
			markup += '<div class="uninav-abs-cbn">';
			$.each(data.sites, function(i, item) {
				markup += '<div class="uninav-container-links">';
				if (item.parent[0].name.toLowerCase() === "abs-cbn") {
					markup += '<div class="parent-uninav '+item.parent[0].name.toLowerCase()+'">';
						markup += '<i class="' + item.parent[0].icon + '"></i>';
						markup += '<a href="//abs-cbn.com" target="_blank" class="uninav-abs-cbn-link">' + item.parent[0].name + '</a>';
					markup += '</div>';
				} else {
					markup += '<div class="parent-uninav '+item.parent[0].name.toLowerCase()+'">';
						markup += '<i class="' + item.parent[0].icon + '"></i>';
						markup += '<a href="javascript:void(0);">' + item.parent[0].name + '</a>';
						markup += '<i class="fa fa-chevron-down toggle-uninav-links"></i>';
					markup += '</div>';
				}
				  markup += '<div class="child-uninav">';
					$.each(item.children, function(i, item) {
					  	markup += '<span><a href="' + item.link + '" target="_blank" class="'+item.clsName+'">' + item.name + '</a></span>';
					});
				  markup += '</div>';
				markup += '</div>';
			});
			markup += '</div>';
		markup += '</div>';
		markup += '<div class="my-blocker opac"></div>';

		$('body').append(markup);
	});


	$(document).on('click', '.sso-navigation', function() {
		$('.sso-sidebar-menu').addClass('inactive');
	});
});
