import store from './store'

// const BasicInfo = () => import('./views/BasicInfo.vue');
// const Address = () => import('./views/Address.vue');
// const Contacts = () => import('./views/Contacts.vue');
// const Accounts = () => import('./views/Accounts.vue');
// const Saved = () => import('./views/Saved.vue');

import BasicInfo from './views/BasicInfo.vue';
import Address from './views/Address.vue';
import Contacts from './views/Contacts.vue';
import Accounts from './views/Accounts.vue';
import Saved from './views/Saved.vue';
import OnBoarding from './views/OnBoarding.vue';

export const routes = [
    {
        path: '/',
        redirect: '/basic-info'
    },
    {
        name: 'basic-info',
        path: '/basic-info',
        component: BasicInfo,
        meta: {
            title: 'Basic Info - Kapamilya Accounts'
        }
    },
    {
        name: 'address',
        path: '/address',
        component: Address,
        meta: {
            title: 'Address - Kapamilya Accounts'
        }
    },
    {
        name: 'contacts',
        path: '/contacts',
        component: Contacts,
        meta: {
            title: 'Contacts - Kapamilya Accounts'
        }
    },
    {
        name: 'accounts',
        path: '/accounts',
        component: Accounts,
        meta: {
            title: 'Accounts - Kapamilya Accounts'
        }
    },
    {
        name: 'saved',
        path: '/saved',
        component: Saved,
        meta: {
            title: 'Saved - Kapamilya Accounts'
        }
    },
    {
        name: 'sso-on-boarding',
        path: '/on-boarding',
        component: OnBoarding,
        meta: {
            title: 'On Boarding - Kapamilya Accounts'
        }
    }
];
