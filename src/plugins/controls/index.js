import _ from 'lodash';

export default {
    install: function(Vue) {

        var HAS_ERROR = "has-error";
        var ACTIVE_INPUT = "active-input";

        var floatingLabel = (element) => {
            return element.querySelector('.floating-label');
        }

        Vue.directive('input-active', {
            inserted: (el, binding, vnode) => {
                if (el.parentElement) {
                    el.addEventListener('focus', function() {
                        floatingLabel(el.parentElement).classList.add(ACTIVE_INPUT);
                    });
    
                    el.addEventListener('blur', function() {
                        if (_.isNull(el.value) || _.isEmpty(el.value)) {
                            floatingLabel(el.parentElement).classList.remove(ACTIVE_INPUT);
                        }
                    });
                }
            },
            componentUpdated: (el, binding, vnode) => {
                if (el.parentElement) {
                    if (!_.isNull(binding.value) && !_.isEmpty(binding.value)) {
                        floatingLabel(el.parentElement).classList.add(ACTIVE_INPUT);
                    }
                }
            }
        });

        Vue.directive('has-error', {
            componentUpdated: (el, binding, vnode) => {
                const errors = vnode.context.errors;
                if (errors.any()) {
                    if (errors.first(binding.value)) {
                        el.classList.add(HAS_ERROR);
                        return;
                    }
                }
                
                el.classList.remove(HAS_ERROR);
            }
        })
    }
}