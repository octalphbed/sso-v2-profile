const Gigya = {
    install: function(Vue, gigya) {

        // gigya.accounts
        Vue.prototype.$accounts = gigya.accounts;

        // gigya.socialize
        Vue.prototype.$socialize = gigya.socialize;
    }
}

export default Gigya;