import _ from 'lodash';
import axios from 'axios';
import store from '../store';
import FormField from './FormField.vue';
import { Validator } from 'vee-validate';

export default (function(Vue, options) {

    Vue.component(FormField.name, FormField);

    Object.defineProperty(Vue.prototype, "$user", {
        get: function() {
            return store.getters.user;
        }
    });

    Object.defineProperty(Vue.prototype, "$accounts", {
        get: function() {
            return gigya.accounts;
        }
    });
    
    Object.defineProperty(Vue.prototype, "$socialize", {
        get: function() {
            return gigya.socialize;
        }
    });

    Validator.extend('unique-username', {
        validate: (value, [current]) => new Promise((resolve) => {
            if (current === value) {
                return resolve({
                    valid: true
                });
            } else {
                gigya.accounts.isAvailableLoginID({
                    loginID: value,
                    callback: (response) => {
                        return resolve({
                            valid: response.isAvailable,
                            data: {
                                message: `${value} is arleady taken.`
                            }
                        });
                    }
                });
            }
        }),

        getMessage: (field, params, data) => data.message
    });

    Validator.extend('special-char', {
        validate: (value, [current]) => new Promise((resolve) => {
            let notAllowedChars = new RegExp('[ `~!#$%^&*()+ÀÈÌÒÙàèìòùÁÉÍÓÚÝáéíóúýÂÊÎÔÛâêîôûÃÑñÕãõÄËÏÖÜŸäëïöüŸ¡¿çÇŒœßØøÅåÆæÞþÐð{}\[;:"|\\\]<>,/]', 'g');

            if (current === value) {
                return resolve({
                    valid: true
                });
            } else {
                let isValid = notAllowedChars.exec(value) ? false : true;
                return resolve({
                    valid: isValid,
                    data: {
                        message: `Special character not allowed.`
                    }
                });
            }
        }),

        getMessage: (field, params, data) => data.message
    });

    Validator.extend('invalid-input', {
        validate: (value, [current]) => new Promise((resolve) => {
            if (current === value) {
                return resolve({
                    valid: true
                });
            } else {
                let isValid = !/[a-zA-Z]/.test(value);
                return resolve({
                    valid: !isValid,
                    data: {
                        message: `Invalid Kapamilya name.`
                    }
                });
            }
        }),

        getMessage: (field, params, data) => data.message
    });

 
    Validator.extend("unique-secondaryemail", {
        validate: (value) => axios.post("/api/Account/secondaryEmailAvailable", { email: value }).then((response) => {
            return {
                valid: response.data.available,
                data: {
                    message: "Email address already exists."
                }
            }
        }),

        getMessage: (field, params, data) => data.message
    });

});