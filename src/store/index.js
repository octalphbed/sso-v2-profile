import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import { guid } from 'utils/guid'
Vue.use(Vuex);

import { 
    CHECK_LOGIN, 
    LOGOUT_ACCOUNT, 
    STORE_ACCOUNT_INFO, 
    STORE_LOGOUT_ACCOUNT,
    STORE_BIRTHDATE,
    STORE_PHOTOURL,
    STORE_SECONDARYEMAIL,
    GET_ACOUNTRIES,
    STORE_COUNTRIES,
    GET_TOOLTIP,
    STORE_TOOLTIP,
    GET_LINKS,
    STORE_LINKS,
    GET_POINTS,
    STORE_POINTS,
    STORE_MOBILENUMBER,
    STORE_READLIST,
    STORE_ADDRESS
} from './vars';

const state = {
    accountInfo: null,
    redirectURL: window.REDIRECT_REFERRER,
    defaultPhoto: "https://assets.abs-cbn.com/sso/images/unknown.jpg",
    countries: null,
    tooltip: null,
    links: null,
    kapamilyaPoints: 0,
    address: null
}

const getters = {
    isLogin: state => state.accountInfo !== null,
    profilePicture: (state) => {
        const accountInfo = (state.accountInfo || { });
        const profile = (accountInfo.profile || {});

        return profile.photoURL || state.defaultPhoto;
    },
    user: state => {
        const accountInfo = (state.accountInfo || { });
        delete accountInfo.time;
        delete accountInfo.callId;
        delete accountInfo.status;
        delete accountInfo.errorCode;
        delete accountInfo.errorMessage;
        delete accountInfo.statusMessage;
        delete accountInfo.ignoredParams;
        delete accountInfo.requestParams;
        delete accountInfo.operation;

        return accountInfo;
    },
    emails: state => {
        const accountInfo = (state.accountInfo || {});
        const accountData = (accountInfo.data || {});
        const accountProfile = (accountInfo.profile || {});

        let emails = (accountData.secondaryEmails || []);
        
        let isExist = _.find(accountData.secondaryEmails, function(o) { return o.email === accountProfile.email; });

        if (!isExist) {
            if (accountProfile.email) {
                emails.push({
                    email: accountProfile.email,
                    createdDate: accountInfo.created,
                    verifiedDate: accountInfo.verified,
                    isVerified: true,
                    IsPrimary: true,
                    id: guid(),
                    primaryDate: accountInfo.verified
                });
            }
        }

        emails = _.sortBy(emails, [function(o) { return o.IsPrimary; }]);

        return emails;
    },
    mobileNumbers: state => {
        const accountInfo = (state.accountInfo || {});
        const accountData = (accountInfo.data || {});

        let mobileNumbers = (accountData.mobile || []);
        
        return mobileNumbers;
    },
    abscbnMobiles: state => {
        const accountInfo = (state.accountInfo || {});
        const accountData = (accountInfo.data || {});
        const mobile = (accountData.mobile || {});
        
        let abscbnNumbers = [];
        
        var abscbn = _.find(mobile, function(o) { return o.serviceProvider === 'abs-cbn' });

        if (abscbn) {
            abscbnNumbers.push(abscbn);
        }

        return abscbnNumbers;
    },
    tvPlus: state => {
        const accountInfo = (state.accountInfo || {});
        const accountData = (accountInfo.data || {});
        
        let tvplus = (accountData.dtt || []);
        
        return tvplus;
    },
    skyAccount: state => {
        const accountInfo = (state.accountInfo || {});
        const accountData = (accountInfo.data || {});

        let skyAccount = (accountData.sky || []);
        
        return skyAccount;
    },

    readList: state => {
        const accountInfo = (state.accountInfo || {});
        const accountData = (accountInfo.data || {});

        let readList = (accountData.readList || []);
        
        return readList;
    },

    redirectURL: state => state.redirectURL || process.env.VUE_APP_DEFAULT_REFERRER
}

const mutations = {
    [STORE_ACCOUNT_INFO] (state, response) {
        state.accountInfo = response;
    },

    [STORE_LOGOUT_ACCOUNT] (state) {
        state.accountInfo = null;
    },

    [STORE_BIRTHDATE] (state, data) {
        state.accountInfo.profile.birthDay = data.birthDay;
        state.accountInfo.profile.birthMonth = data.birthMonth;
        state.accountInfo.profile.birthYear = data.birthYear;
    },

    [STORE_PHOTOURL](state, photoURL) {
        state.accountInfo.profile.photoURL = photoURL;
    },

    [STORE_SECONDARYEMAIL] (state, data) {
        state.accountInfo.data.secondaryEmails = data;
    },

    [STORE_ADDRESS] (state, data) {
        state.address = data;
    },

    [STORE_MOBILENUMBER] (state, data) {
        state.accountInfo.data.mobile = data;
    },

    [STORE_COUNTRIES] (state, data) {
        state.countries = data;
    },

    [STORE_TOOLTIP] (state, data) {
        state.tooltip = data;
    },

    [STORE_LINKS] (state, data) {
        state.links = data;
    },

    [STORE_POINTS] (state, data) {
        state.kapamilyaPoints = data;
    },

    [STORE_READLIST] (state, data) {
        state.accountInfo.data.readList = data;
    }
}

const actions = ({
    [CHECK_LOGIN]: ({ commit, dispatch }, accounts) => {
        return new Promise((resolve, reject) => {
            accounts.getAccountInfo({
                include: 'profile, loginIDs, data',
                callback: function(response) {
                    resolve(response.status !== 'FAIL');
                    if (response.status !== 'FAIL') {
                        commit(STORE_ACCOUNT_INFO, response);

                        //let addr = (response.data.ph || []);
                        commit(STORE_ADDRESS, response.data.ph);

                        // Get Kapamilya Points
                        // 1eed046bfccd44fca46a5e75d8825c58 - sample uid that has points value
                        axios.get('/api/Profile/KapamilyaPoints?UID='+response.UID).then((res) => {
                            if (res.data) {
                                if (res.data.errorCode === 5001) {
                                    commit(STORE_POINTS, 'Currently Unavailable');
                                } else {
                                    commit(STORE_POINTS, Math.round(res.data.points) + ' pts');
                                }
                            }
                        }).catch(function (error) {
                            console.log(error);
                        });
                    }
                }
            })
        });
    },
    [LOGOUT_ACCOUNT]: ({ commit }, socialize) => {
        return new Promise((resolve, reject) => {
            if (socialize) {
                socialize.logout({
                    callback: (response) => {
                        if (response.errorCode === 0) {
                            commit(STORE_LOGOUT_ACCOUNT);
                            resolve();
                        } else {
                            reject();
                        }
                    }
                })
            } else { 
                commit(STORE_LOGOUT_ACCOUNT);
                resolve();
            }
        })
    },
    [GET_ACOUNTRIES]: ({ commit }) => {
        axios.get("/api/Profile/Countries").then(response => {
            commit(STORE_COUNTRIES, response.data)
        });
    },
    [GET_TOOLTIP]: ({ commit }) => {
        axios.get("/api/Profile/Tooltip").then(response => {
            commit(STORE_TOOLTIP, response.data)
        });
    },
    [GET_LINKS]: ({ commit }) => {
        axios.get("/api/Profile/Links").then(response => {
           commit(STORE_LINKS, response.data) 
        });
    },

    [GET_POINTS]: ({ commit }) => {}
})

export default new Vuex.Store({
    state,
    getters,
    mutations,
    actions
})

