import Vue from 'vue';
import axios from 'axios';
import VeeValidate from 'vee-validate';
import VuePromiseBtn from 'vue-promise-btn';
import { sync } from 'vuex-router-sync'

import App from './App.vue';
import store from './store';
import router from './router';
import Plugins from './plugins';
import GigyaSdk from './plugins/gigya';
import Controls from './plugins/controls';
import './styles/sso-v2.1.min.scss';
import './utils'
import 'vue-promise-btn/dist/vue-promise-btn.css'
import 'simplebar/dist/simplebar.css';
import 'es6-promise/auto';

Vue.use(Controls);
Vue.use(VeeValidate);
Vue.use(VuePromiseBtn);
Vue.use(Plugins, { gigya });

sync(store, router)
Vue.prototype.$http = axios
Vue.config.productionTip = false

const sso = new Vue({
    store,
    router,
    render: h => h(App)
}).$mount('#sso-profile');

const isDev = process.env.NODE_ENV === 'development';

var tOut = setTimeout(function() {
    sso.$store.dispatch("CHECK_LOGIN", gigya.accounts).then((isLogin) => {
        sso.$children[0].hideLoader();

        if (!isLogin && !isDev) {
            window.location.href = store.getters.redirectURL;
        }
    });

    gigya.accounts.addEventHandlers({
        onLogout: function() {
            window.location.href = store.getters.redirectURL;
        }
    })

    clearTimeout(tOut);
}, 500);