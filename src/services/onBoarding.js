import axios from 'axios';

// OCP KEY
let HEADERS = { headers: { 'Ocp-Apim-Subscription-Key': process.env.VUE_APP_API_ON_BOARDING_OCP_KEY } };

/*  API BASE URL */
let VUE_APP_API_ON_BOARDING = process.env.VUE_APP_API_ON_BOARDING;

const ON_BOARDING = {
    GET: ( api_url ) => {
        return new Promise( (resolve, reject) => {
            try {
                const res = axios({
                                method: 'GET',
                                baseURL : VUE_APP_API_ON_BOARDING,
                                url: api_url,
                                headers: HEADERS
                            });
                resolve(res.data);
            } catch (e) {
                reject('An error occurred sending the request');
            }
        });
        
    },

    POST : ( api_url, data ) => {
        return new Promise( (resolve, reject) => {
            try {
                const res = axios({
                                method: 'POST',
                                baseURL : VUE_APP_API_ON_BOARDING,
                                url: api_url,
                                data: data,
                                headers: HEADERS                            
                            })
                resolve(res.data);
            } catch (e) {
                reject('An error occurred sending the request');
            }
        });
        
    }
}

export default ON_BOARDING;