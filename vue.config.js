const path = require('path')

module.exports = {
    devServer: {
        proxy: {
            '/api': {
                target: 'https://kapamilya-accounts.abs-cbn.com',
                changeOrigin: true,
                ws: true
            }
        }
    },

    baseUrl: process.env.VUE_APP_BASE_URL,

    chainWebpack: config => {
        config.resolve.alias
            .set("assets", path.resolve(__dirname, "./src/assets"))
            .set("components", path.resolve(__dirname, "./src/components"))
            .set("utils", path.resolve(__dirname, "./src/utils"))
            .set("styles", path.resolve(__dirname, "./src/styles"));

        if(config.plugins.has('extract-css')) {
          const extractCSSPlugin = config.plugin('extract-css')
          extractCSSPlugin && extractCSSPlugin.tap(() => [{
            filename: 'css/[name].css',
            chunkFilename: 'css/[name].css'
          }])
        }
    },
    configureWebpack: {
        output: {
          filename: 'js/[name].js',
          chunkFilename: 'js/[name].js'
        }
    }
}